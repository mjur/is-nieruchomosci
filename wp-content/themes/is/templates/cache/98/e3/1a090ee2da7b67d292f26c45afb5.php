<?php

/* helpers/topbar.twig */
class __TwigTemplate_98e31a090ee2da7b67d292f26c45afb5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((($this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_theme_mod", array(0 => "topbar_breadcrumb_is_hidden"), "method") == "") || ($this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_theme_mod", array(0 => "topbar_user_links_is_hidden"), "method") == ""))) {
            // line 2
            echo "    <div class=\"breadcrumb-wrapper\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"span12\">
                    ";
            // line 7
            echo "                    ";
            if (($this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_theme_mod", array(0 => "topbar_breadcrumb_is_hidden"), "method") == "")) {
                // line 8
                echo "                        <div class=\"breadcrumb pull-left\">
                            ";
                // line 9
                if ($this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "function_exists", array(0 => "bcn_display"), "method")) {
                    // line 10
                    echo "                                ";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "bcn_display"), "html", null, true);
                    echo "
                            ";
                }
                // line 12
                echo "                        </div><!-- /.breadcrumb -->
                    ";
            }
            // line 14
            echo "
                    ";
            // line 16
            echo "                    ";
            if (($this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_theme_mod", array(0 => "topbar_user_links_is_hidden"), "method") == "")) {
                // line 17
                echo "                        <div class=\"account pull-right\">
                            ";
                // line 18
                if ($this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "is_user_logged_in", array(), "method")) {
                    // line 19
                    echo "                                ";
                    echo (isset($context["authenticated_menu"]) ? $context["authenticated_menu"] : null);
                    echo "
                            ";
                } else {
                    // line 21
                    echo "                                ";
                    echo (isset($context["anonymous_menu"]) ? $context["anonymous_menu"] : null);
                    echo "
                            ";
                }
                // line 22
                echo "                        
                        </div>
                    ";
            }
            // line 25
            echo "                </div><!-- /.span12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.breadcrumb-wrapper -->
";
        }
    }

    public function getTemplateName()
    {
        return "helpers/topbar.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  68 => 22,  62 => 21,  56 => 19,  51 => 17,  48 => 16,  45 => 14,  41 => 12,  33 => 9,  30 => 8,  27 => 7,  21 => 2,  334 => 144,  332 => 143,  330 => 142,  328 => 141,  325 => 140,  320 => 138,  300 => 120,  294 => 117,  290 => 116,  287 => 115,  284 => 114,  281 => 112,  277 => 110,  271 => 107,  268 => 106,  265 => 105,  262 => 103,  255 => 99,  251 => 98,  248 => 97,  245 => 96,  242 => 94,  240 => 93,  236 => 91,  233 => 90,  227 => 87,  224 => 86,  221 => 85,  218 => 84,  215 => 82,  212 => 81,  205 => 77,  199 => 76,  196 => 75,  193 => 74,  190 => 73,  185 => 69,  177 => 67,  169 => 65,  167 => 64,  161 => 63,  158 => 62,  153 => 59,  141 => 49,  139 => 48,  110 => 42,  104 => 39,  101 => 38,  97 => 36,  95 => 35,  92 => 34,  84 => 32,  82 => 31,  79 => 30,  73 => 25,  71 => 27,  66 => 25,  60 => 22,  54 => 18,  49 => 17,  42 => 13,  35 => 10,  29 => 6,  23 => 3,  19 => 1,);
    }
}
