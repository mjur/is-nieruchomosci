<?php

/* helpers/footer.twig */
class __TwigTemplate_b1ae578d8bf5414f42044095bb4eb6e9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "            </div><!-- /#wrapper-inner -->

            ";
        // line 3
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_sidebar", array(0 => "bottom"), "method"), "html", null, true);
        echo "
        </div><!-- /#wrapper-outer -->
        
        <div id=\"footer-wrapper\">
            ";
        // line 7
        if (((($this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "is_active_sidebar", array(0 => "footer-1"), "method") || $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "is_active_sidebar", array(0 => "footer-2"), "method")) || $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "is_active_sidebar", array(0 => "footer-3"), "method")) || $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "is_active_sidebar", array(0 => "footer-4"), "method"))) {
            // line 8
            echo "                <div id=\"footer-top\">
                    <div id=\"footer-top-inner\" class=\"container\">
                        <div class=\"row\">
                            <div class=\"span3\">
                                ";
            // line 12
            if ((!$this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "dynamic_sidebar", array(0 => "footer-1"), "method"))) {
                // line 13
                echo "                                    ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_sidebar", array(0 => "footer-1"), "method"), "html", null, true);
                echo "
                                ";
            }
            // line 15
            echo "                            </div>
    
                            <div class=\"span3\">
                                ";
            // line 18
            if ((!$this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "dynamic_sidebar", array(0 => "footer-2"), "method"))) {
                // line 19
                echo "                                    ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_sidebar", array(0 => "footer-2"), "method"), "html", null, true);
                echo "
                                ";
            }
            // line 21
            echo "                            </div>
    
                            <div class=\"span3\">
                                ";
            // line 24
            if ((!$this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "dynamic_sidebar", array(0 => "footer-3"), "method"))) {
                // line 25
                echo "                                    ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_sidebar", array(0 => "footer-3"), "method"), "html", null, true);
                echo "
                                ";
            }
            // line 27
            echo "                            </div>
    
                            <div class=\"span3\">
                                ";
            // line 30
            if ((!$this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "dynamic_sidebar", array(0 => "footer-4"), "method"))) {
                // line 31
                echo "                                    ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_sidebar", array(0 => "footer-4"), "method"), "html", null, true);
                echo "
                                ";
            }
            // line 33
            echo "                            </div>
                        </div><!-- /.row -->
                    </div><!-- /#footer-top-inner -->
                </div><!-- /#footer-top -->
            ";
        }
        // line 38
        echo "    
            <div id=\"footer\" class=\"footer container\">
                <div id=\"footer-inner\">
                    <div class=\"row\">
                        <div class=\"span6 copyright\">
                            ";
        // line 43
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_sidebar", array(0 => "footer-bottom-left"), "method"), "html", null, true);
        echo "
                        </div><!-- /.copyright -->
    
                        <div class=\"span6 share\">
                            <div class=\"content\">
                                <ul class=\"menu nav\">
                                    ";
        // line 49
        if ((!$this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "aviators_settings_get_value", array(0 => "socialize", 1 => "facebook", 2 => "is_hidden"), "method"))) {
            // line 50
            echo "                                        <li class=\"first leaf\">
                                            <a href=\"";
            // line 51
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "aviators_settings_get_value", array(0 => "socialize", 1 => "facebook", 2 => "link"), "method"), "html", null, true);
            echo "\" class=\"facebook\">Facebook</a>
                                        </li>
                                    ";
        }
        // line 54
        echo "    
                                    ";
        // line 55
        if ((!$this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "aviators_settings_get_value", array(0 => "socialize", 1 => "flickr", 2 => "is_hidden"), "method"))) {
            // line 56
            echo "                                        <li class=\"leaf\"><a href=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "aviators_settings_get_value", array(0 => "socialize", 1 => "flickr", 2 => "link"), "method"), "html", null, true);
            echo "\" class=\"flickr\">Flickr</a></li>
                                    ";
        }
        // line 58
        echo "    
                                    ";
        // line 59
        if ((!$this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "aviators_settings_get_value", array(0 => "socialize", 1 => "google_plus", 2 => "is_hidden"), "method"))) {
            // line 60
            echo "                                        <li class=\"leaf\"><a href=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "aviators_settings_get_value", array(0 => "socialize", 1 => "google_plus", 2 => "link"), "method"), "html", null, true);
            echo "\" class=\"google\">Google+</a></li>
                                    ";
        }
        // line 62
        echo "    
                                    ";
        // line 63
        if ((!$this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "aviators_settings_get_value", array(0 => "socialize", 1 => "linkedin", 2 => "is_hidden"), "method"))) {
            // line 64
            echo "                                        <li class=\"leaf\"><a href=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "aviators_settings_get_value", array(0 => "socialize", 1 => "linkedin", 2 => "link"), "method"), "html", null, true);
            echo "\" class=\"linkedin\">LinkedIn</a></li>
                                    ";
        }
        // line 66
        echo "    
                                    ";
        // line 67
        if ((!$this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "aviators_settings_get_value", array(0 => "socialize", 1 => "twitter", 2 => "is_hidden"), "method"))) {
            // line 68
            echo "                                        <li class=\"leaf\"><a href=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "aviators_settings_get_value", array(0 => "socialize", 1 => "twitter", 2 => "link"), "method"), "html", null, true);
            echo "\" class=\"twitter\">Twitter</a></li>
                                    ";
        }
        // line 70
        echo "    
                                    ";
        // line 71
        if ((!$this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "aviators_settings_get_value", array(0 => "socialize", 1 => "vimeo", 2 => "is_hidden"), "method"))) {
            // line 72
            echo "                                        <li class=\"last leaf\"><a href=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "aviators_settings_get_value", array(0 => "socialize", 1 => "vimeo", 2 => "link"), "method"), "html", null, true);
            echo "\" class=\"vimeo\">Vimeo</a></li>
                                    ";
        }
        // line 74
        echo "                                </ul>
                            </div><!-- /.content -->
                        </div><!-- /.span6 -->
                    </div><!-- /.row -->
                </div><!-- /#footer-inner -->
            </div><!-- /#footer -->
        </div><!-- /#footer-wrapper -->
        
    </div><!-- /#wrapper -->

    ";
        // line 84
        if (($this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_theme_mod", array(0 => "general_palette_is_hidden"), "method") == "")) {
            // line 85
            echo "        ";
            $this->env->loadTemplate("helpers/palette.twig")->display($context);
            // line 86
            echo "    ";
        }
        // line 87
        echo "
    ";
        // line 88
        if ($this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "aviators_settings_get_value", array(0 => "google_analytics", 1 => "tracking", 2 => "code"), "method")) {
            // line 89
            echo "        <script type=\"text/javascript\">
            var _gaq=[[\"_setAccount\", \"";
            // line 90
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "aviators_settings_get_value", array(0 => "google_analytics", 1 => "tracking", 2 => "code"), "method"), "html", null, true);
            echo "\"],[\"_trackPageview\"]];
            (function(d,t){ var g=d.createElement(t),s=d.getElementsByTagName(t)[0];g.async=1;
                g.src=\"//www.google-analytics.com/ga.js\";
                s.parentNode.insertBefore(g,s) }(document,\"script\"));
        </script>
    ";
        }
        // line 96
        echo "
    ";
        // line 97
        echo twig_escape_filter($this->env, aviators_templates_helpers_wp_footer(), "html", null, true);
        echo "
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "helpers/footer.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  214 => 97,  211 => 96,  202 => 90,  199 => 89,  197 => 88,  194 => 87,  191 => 86,  188 => 85,  186 => 84,  174 => 74,  168 => 72,  166 => 71,  163 => 70,  157 => 68,  155 => 67,  152 => 66,  146 => 64,  144 => 63,  141 => 62,  135 => 60,  133 => 59,  130 => 58,  124 => 56,  122 => 55,  119 => 54,  113 => 51,  110 => 50,  108 => 49,  99 => 43,  92 => 38,  85 => 33,  79 => 31,  77 => 30,  72 => 27,  66 => 25,  64 => 24,  59 => 21,  53 => 19,  51 => 18,  46 => 15,  40 => 13,  38 => 12,  32 => 8,  30 => 7,  23 => 3,  19 => 1,);
    }
}
