<?php

/* helpers/header.twig */
class __TwigTemplate_7da7b381adde7a208f03c8b2853f77d1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<!--[if IE 7]>
<html class=\"ie ie7\" ";
        // line 3
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "language_attributes", array(), "method"), "html", null, true);
        echo ">
<![endif]-->
<!--[if IE 8]>
<html class=\"ie ie8\" ";
        // line 6
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "language_attributes", array(), "method"), "html", null, true);
        echo ">
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html ";
        // line 9
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "language_attributes", array(), "method"), "html", null, true);
        echo ">
<!--<![endif]-->

<head>
    <meta charset=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "bloginfo", array(0 => "charset"), "method"), "html", null, true);
        echo "\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <meta name=\"author\" content=\"Aviators, http://themes.byaviators.com\">

    <link rel=\"shortcut icon\" href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_stylesheet_directory_uri", array(), "method"), "html", null, true);
        echo "/assets/img/favicon.png\" type=\"image/png\">
    <link rel=\"profile\" href=\"http://gmpg.org/xfn/11\">
    <link rel=\"pingback\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "bloginfo", array(0 => "pingback_url"), "method"), "html", null, true);
        echo "\">

    <!--[if lt IE 9]>
        <script src=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_template_directory_uri", array(), "method"), "html", null, true);
        echo "/assets/js/html5.js\" type=\"text/javascript\"></script>
    <![endif]-->

    ";
        // line 25
        echo twig_escape_filter($this->env, aviators_templates_helpers_wp_head(), "html", null, true);
        echo "

    ";
        // line 27
        if ($this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "is_singular", array(), "method")) {
            // line 28
            echo "        ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "wp_enqueue_script", array(0 => "comment-reply"), "method"), "html", null, true);
            echo "
    ";
        }
        // line 30
        echo "
    ";
        // line 31
        if ($this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_theme_mod", array(0 => "general_variant"), "method")) {
            // line 32
            echo "        <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_template_directory_uri", array(), "method"), "html", null, true);
            echo "/assets/css/";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_theme_mod", array(0 => "general_variant"), "method"), "html", null, true);
            echo "\" type=\"text/css\">
    ";
        }
        // line 34
        echo "
    ";
        // line 35
        if (($this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_theme_mod", array(0 => "general_palette_is_hidden"), "method") == "")) {
            // line 36
            echo "        <link rel=\"stylesheet\" href=\"#\" type=\"text/css\" id=\"color-variant\">
    ";
        }
        // line 38
        echo "
    <title>";
        // line 39
        echo $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "wp_title", array(0 => "|", 1 => false, 2 => "right"), "method");
        echo "</title>
</head>

<body class=\"";
        // line 42
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_body_class"));
        foreach ($context['_seq'] as $context["_key"] => $context["class"]) {
            echo twig_escape_filter($this->env, (isset($context["class"]) ? $context["class"] : null), "html", null, true);
            echo " ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['class'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_theme_mod", array(0 => "header_variant"), "method"), "html", null, true);
        echo " color-";
        echo twig_escape_filter($this->env, (isset($context["color_class"]) ? $context["color_class"] : null), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_theme_mod", array(0 => "general_pattern"), "method"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_theme_mod", array(0 => "general_layout"), "method"), "html", null, true);
        echo " ";
        if ($this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_theme_mod", array(0 => "header_position_is_fixed"), "method")) {
            echo "header-fixed";
        }
        echo "\">

<div id=\"wrapper-outer\">
    <!-- /.top -->
    <div class=\"header-top-wrapper\">
        <div class=\"header-top\">
            ";
        // line 48
        $this->env->loadTemplate("helpers/topbar.twig")->display($context);
        // line 49
        echo "
            <!-- HEADER -->
            <div id=\"header-wrapper\">
                <div id=\"header\">
                    <div id=\"header-inner\">
                        <div class=\"container\">
                            <div class=\"navbar\">
                                <div class=\"navbar-inner\">
                                    <div class=\"row\">
                                        <div class=\"logo-wrapper\">
                                            <a href=\"#nav\" id=\"btn-nav\">";
        // line 59
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "__", array(0 => "Toggle navigation", 1 => "aviators"), "method"), "html", null, true);
        echo "</a>

                                            ";
        // line 62
        echo "                                            <div class=\"logo\">
                                                <a href=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_bloginfo", array(0 => "wpurl"), "method"), "html", null, true);
        echo "\" title=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "__", array(0 => "Home"), "method"), "html", null, true);
        echo "\">
                                                    ";
        // line 64
        if ($this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_theme_mod", array(0 => "header_logo"), "method")) {
            // line 65
            echo "                                    
                                                    ";
        } else {
            // line 67
            echo "                                                        <img src=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_template_directory_uri", array(), "method"), "html", null, true);
            echo "/assets/img/logo.png\" alt=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "__", array(0 => "Home", 1 => "aviators"), "method"), "html", null, true);
            echo "\">
                                                    ";
        }
        // line 69
        echo "                                                </a>
                                            </div><!-- /.logo -->

                                            ";
        // line 73
        echo "                                            ";
        if (($this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_theme_mod", array(0 => "header_title_is_hidden"), "method") == "")) {
            // line 74
            echo "                                                ";
            if ($this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_bloginfo", array(0 => "name"), "method")) {
                // line 75
                echo "                                                    <div class=\"site-name\">
                                                        <a href=\"";
                // line 76
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_bloginfo", array(0 => "wpurl"), "method"), "html", null, true);
                echo "\" title=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "__", array(0 => "Home"), "method"), "html", null, true);
                echo "\" class=\"brand\">
                                                            ";
                // line 77
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_bloginfo", array(0 => "name"), "method"), "html", null, true);
                echo "
                                                        </a>
                                                    </div><!-- /.site-name -->
                                                ";
            }
            // line 81
            echo "                                            ";
        }
        // line 82
        echo "
                                            ";
        // line 84
        echo "                                            ";
        if (($this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_theme_mod", array(0 => "header_description_is_hidden"), "method") == "")) {
            // line 85
            echo "                                                ";
            if ($this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_bloginfo", array(0 => "description"), "method")) {
                // line 86
                echo "                                                    <div class=\"site-slogan\">
                                                        <span>";
                // line 87
                echo $this->env->getExtension('html_decode_twig_extension')->htmldecode($this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_bloginfo", array(0 => "description"), "method"));
                echo "</span>
                                                    </div><!-- /.site-slogan -->
                                                ";
            }
            // line 90
            echo "                                            ";
        }
        // line 91
        echo "                                        </div><!-- /.logo-wrapper -->

                                        ";
        // line 93
        if ((($this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_theme_mod", array(0 => "header_email_is_hidden"), "method") == "") || ($this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_theme_mod", array(0 => "header_phone_is_hidden"), "method") == ""))) {
            // line 94
            echo "                                            <div class=\"info\">
                                                ";
            // line 96
            echo "                                                ";
            if (($this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_theme_mod", array(0 => "header_email_is_hidden"), "method") == "")) {
                // line 97
                echo "                                                    <div class=\"site-email\">
                                                        <a href=\"mailto:";
                // line 98
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_option", array(0 => "header_email", 1 => "info@byaviators.com"), "method"), "html", null, true);
                echo "\">
                                                            ";
                // line 99
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_option", array(0 => "header_email", 1 => "info@byaviators.com"), "method"), "html", null, true);
                echo "
                                                        </a>
                                                    </div><!-- /.site-email -->
                                                ";
            }
            // line 103
            echo "
                                                ";
            // line 105
            echo "                                                ";
            if (($this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_theme_mod", array(0 => "header_phone_is_hidden"), "method") == "")) {
                // line 106
                echo "                                                    <div class=\"site-phone\">
                                                        <span>";
                // line 107
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_option", array(0 => "header_phone", 1 => "333-444-555"), "method"), "html", null, true);
                echo "</span>
                                                    </div><!-- /.site-phone -->
                                                ";
            }
            // line 110
            echo "                                            </div><!-- /.info -->
                                        ";
        }
        // line 112
        echo "
                                        ";
        // line 114
        echo "                                        ";
        if (($this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_theme_mod", array(0 => "header_call_to_action_is_hidden"), "method") == "")) {
            // line 115
            echo "                                            <a class=\"btn btn-primary btn-large list-your-property arrow-right\"
                                                href=\"";
            // line 116
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_theme_mod", array(0 => "header_call_to_action_url", 1 => "/wp-admin/edit.php?post_type=property"), "method"), "html", null, true);
            echo "\">
                                                ";
            // line 117
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_theme_mod", array(0 => "header_call_to_action_text", 1 => $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "__", array(0 => "List your property"), "method")), "method"), "html", null, true);
            echo "
                                            </a>
                                        ";
        }
        // line 120
        echo "                                    </div><!-- /.row -->
                                </div><!-- /.navbar-inner -->
                            </div><!-- /.navbar -->
                        </div><!-- /.container -->
                    </div><!-- /#header-inner -->
                </div><!-- /#header -->
            </div><!-- /#header-wrapper -->
        </div>
    </div>    
    <div id=\"wrapper\">
        <div id=\"wrapper-inner\">
            <!-- /.top-wrapper -->

            <!-- NAVIGATION -->
            <div id=\"navigation\">
                <div class=\"container\">
                    <div class=\"navigation-wrapper\">
                        <div class=\"navigation clearfix-normal\">
                            ";
        // line 138
        echo (isset($context["main_menu"]) ? $context["main_menu"] : null);
        echo "

                            ";
        // line 140
        if ($this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "dynamic_sidebar", array(0 => "navigation-right"), "method")) {
        }
        // line 141
        echo "                            ";
        // line 142
        echo "                                ";
        // line 143
        echo "                            ";
        // line 144
        echo "                        </div><!-- /.navigation -->
                    </div><!-- /.navigation-wrapper -->
                </div><!-- /.container -->
            </div><!-- /.navigation -->";
    }

    public function getTemplateName()
    {
        return "helpers/header.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  330 => 144,  328 => 143,  326 => 142,  324 => 141,  321 => 140,  316 => 138,  296 => 120,  290 => 117,  286 => 116,  283 => 115,  280 => 114,  277 => 112,  273 => 110,  267 => 107,  264 => 106,  261 => 105,  258 => 103,  251 => 99,  247 => 98,  244 => 97,  241 => 96,  238 => 94,  236 => 93,  232 => 91,  229 => 90,  223 => 87,  220 => 86,  217 => 85,  214 => 84,  211 => 82,  208 => 81,  201 => 77,  195 => 76,  192 => 75,  189 => 74,  186 => 73,  181 => 69,  173 => 67,  169 => 65,  167 => 64,  161 => 63,  158 => 62,  153 => 59,  141 => 49,  139 => 48,  110 => 42,  104 => 39,  101 => 38,  97 => 36,  95 => 35,  92 => 34,  84 => 32,  82 => 31,  79 => 30,  73 => 28,  71 => 27,  66 => 25,  60 => 22,  54 => 19,  49 => 17,  42 => 13,  35 => 9,  29 => 6,  23 => 3,  19 => 1,);
    }
}
