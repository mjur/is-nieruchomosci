<?php

/* properties/amenities.twig */
class __TwigTemplate_8eae1faa6c9d10808b77aec3b9cf9c8c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((isset($context["amenities"]) ? $context["amenities"] : null)) {
            // line 2
            echo "    <div class=\"row\">
        <div class=\"span12\">
            <h2>";
            // line 4
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "__", array(0 => "General amenities", 1 => "aviators"), "method"), "html", null, true);
            echo "</h2>

            <div class=\"row\">
                ";
            // line 7
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["amenities"]) ? $context["amenities"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["column"]) {
                // line 8
                echo "                    <ul class=\"span2\">
                        ";
                // line 9
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["column"]) ? $context["column"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["amenity"]) {
                    // line 10
                    echo "                            <li class=\"";
                    if ($this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "has_term", array(0 => $this->getAttribute((isset($context["amenity"]) ? $context["amenity"] : null), "term_id"), 1 => "amenities", 2 => $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "ID")), "method")) {
                        echo "checked";
                    } else {
                        echo "plain";
                    }
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["amenity"]) ? $context["amenity"] : null), "name"), "html", null, true);
                    echo "</li>
                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['amenity'], $context['_parent'], $context['loop']);
                $context = array_merge($_parent, array_intersect_key($context, $_parent));
                // line 12
                echo "                    </ul><!-- /.span2 -->
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['column'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 14
            echo "            </div><!-- /.row -->
        </div><!-- /.span12 -->
    </div><!-- /.row -->
";
        }
    }

    public function getTemplateName()
    {
        return "properties/amenities.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 14,  57 => 12,  42 => 10,  35 => 8,  172 => 65,  165 => 62,  161 => 61,  158 => 60,  156 => 59,  153 => 58,  147 => 55,  143 => 54,  140 => 53,  138 => 52,  135 => 51,  129 => 48,  125 => 47,  122 => 46,  120 => 45,  114 => 42,  110 => 41,  103 => 37,  99 => 36,  95 => 34,  90 => 31,  78 => 27,  76 => 26,  71 => 24,  68 => 23,  63 => 21,  56 => 17,  51 => 15,  46 => 13,  36 => 10,  32 => 9,  24 => 4,  92 => 23,  88 => 22,  84 => 29,  66 => 22,  62 => 15,  38 => 9,  29 => 5,  25 => 4,  21 => 2,  19 => 1,  60 => 16,  58 => 13,  55 => 12,  53 => 13,  48 => 14,  45 => 10,  43 => 9,  39 => 7,  37 => 6,  31 => 7,  28 => 3,);
    }
}
