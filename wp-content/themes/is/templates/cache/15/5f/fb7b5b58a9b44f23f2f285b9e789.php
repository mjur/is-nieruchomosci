<?php

/* properties/property-box-small.twig */
class __TwigTemplate_155ffb7b5b58a9b44f23f2f285b9e789 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"property\">
    <div class=\"image\">
        <div class=\"content\">
            <a href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_permalink", array(0 => $this->getAttribute((isset($context["property"]) ? $context["property"] : null), "ID")), "method"), "html", null, true);
        echo "\"></a>
            ";
        // line 5
        if ($this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_the_post_thumbnail", array(0 => $this->getAttribute((isset($context["property"]) ? $context["property"] : null), "ID")), "method")) {
            // line 6
            echo "                ";
            echo $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_the_post_thumbnail", array(0 => $this->getAttribute((isset($context["property"]) ? $context["property"] : null), "ID")), "method");
            echo "
            ";
        } else {
            // line 8
            echo "                <img src=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_template_directory_uri", array(), "method"), "html", null, true);
            echo "/assets/img/property-tmp-small.png\" alt=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["property"]) ? $context["property"] : null), "post_title"), "html", null, true);
            echo "\">
            ";
        }
        // line 10
        echo "        </div><!-- /.content -->

        ";
        // line 12
        if (($this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "aviators_settings_get_value", array(0 => "properties", 1 => "common", 2 => "disable_contract_type_label"), "method") != "on")) {
            // line 13
            echo "            ";
            if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["property"]) ? $context["property"] : null), "meta"), "_property_contract_type"), 0)) {
                // line 14
                echo "                <div class=\"rent-sale\">
                    ";
                // line 15
                if (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["property"]) ? $context["property"] : null), "meta"), "_property_contract_type"), 0) == "sale")) {
                    // line 16
                    echo "                        ";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "__", array(0 => "Sale", 1 => "aviators"), "method"), "html", null, true);
                    echo "
                    ";
                } elseif (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["property"]) ? $context["property"] : null), "meta"), "_property_contract_type"), 0) == "rent")) {
                    // line 18
                    echo "                        ";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "__", array(0 => "Rent", 1 => "aviators"), "method"), "html", null, true);
                    echo "
                    ";
                }
                // line 20
                echo "                </div><!-- /.rent-sale -->
            ";
            }
            // line 22
            echo "        ";
        }
        // line 23
        echo "
        <div class=\"price\">";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "aviators_price_format", array(0 => $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["property"]) ? $context["property"] : null), "meta"), "_property_price"), 0)), "method"), "html", null, true);
        echo "</div><!-- /.price -->

        ";
        // line 26
        if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["property"]) ? $context["property"] : null), "meta"), "_property_reduced"), 0)) {
            // line 27
            echo "            <div class=\"reduced\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "__", array(0 => "Reduced", 1 => "aviators"), "method"), "html", null, true);
            echo "</div><!-- /.reduced -->
        ";
        }
        // line 29
        echo "    </div><!-- /.image -->

    <div class=\"title\">
        <h2><a href=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "get_permalink", array(0 => $this->getAttribute((isset($context["property"]) ? $context["property"] : null), "ID")), "method"), "html", null, true);
        echo "\">
            ";
        // line 33
        if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["property"]) ? $context["property"] : null), "meta"), "_property_title"), 0)) {
            // line 34
            echo "                ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["property"]) ? $context["property"] : null), "meta"), "_property_title"), 0), "html", null, true);
            echo "
            ";
        } else {
            // line 36
            echo "                ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["property"]) ? $context["property"] : null), "post_title"), "html", null, true);
            echo "
            ";
        }
        // line 38
        echo "        </a></h2>
    </div><!-- /.title -->

    <div class=\"location\">";
        // line 41
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["property"]) ? $context["property"] : null), "location"), 0), "name"), "html", null, true);
        echo "</div><!-- /.location -->

    ";
        // line 43
        if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["property"]) ? $context["property"] : null), "meta"), "_property_area"), 0)) {
            // line 44
            echo "        <div class=\"area\">
            <span class=\"key\">";
            // line 45
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "__", array(0 => "Area", 1 => "aviators"), "method"), "html", null, true);
            echo ":</span><!-- /.key -->
            <span class=\"value\">";
            // line 46
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["property"]) ? $context["property"] : null), "meta"), "_property_area"), 0), "html", null, true);
            echo $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "aviators_settings_get_value", array(0 => "properties", 1 => "units", 2 => "area"), "method");
            echo "</span><!-- /.value -->
        </div><!-- /.area -->
    ";
        }
        // line 49
        echo "
    ";
        // line 50
        if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["property"]) ? $context["property"] : null), "meta"), "_property_bathrooms"), 0)) {
            // line 51
            echo "        <div class=\"bathrooms\" title=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "__", array(0 => "Bathrooms", 1 => "aviators"), "method"), "html", null, true);
            echo "\"><div class=\"content\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["property"]) ? $context["property"] : null), "meta"), "_property_bathrooms"), 0), "html", null, true);
            echo "</div></div><!-- /.bathrooms -->
    ";
        }
        // line 52
        echo "0

    ";
        // line 54
        if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["property"]) ? $context["property"] : null), "meta"), "_property_bedrooms"), 0)) {
            // line 55
            echo "        <div class=\"bedrooms\" title=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wp"]) ? $context["wp"] : null), "__", array(0 => "Bedrooms", 1 => "aviators"), "method"), "html", null, true);
            echo "\"><div class=\"content\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["property"]) ? $context["property"] : null), "meta"), "_property_bedrooms"), 0), "html", null, true);
            echo "</div></div><!-- /.bedrooms -->
    ";
        }
        // line 57
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "properties/property-box-small.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  169 => 57,  161 => 55,  159 => 54,  155 => 52,  147 => 51,  145 => 50,  142 => 49,  135 => 46,  131 => 45,  128 => 44,  121 => 41,  93 => 29,  87 => 27,  85 => 26,  80 => 24,  70 => 20,  64 => 18,  50 => 13,  48 => 12,  30 => 6,  82 => 18,  68 => 17,  61 => 13,  58 => 16,  56 => 15,  53 => 14,  21 => 2,  45 => 9,  36 => 8,  27 => 4,  24 => 4,  22 => 2,  19 => 1,  130 => 17,  126 => 43,  122 => 41,  116 => 38,  114 => 38,  110 => 36,  104 => 34,  102 => 33,  98 => 32,  95 => 30,  89 => 27,  86 => 26,  83 => 25,  79 => 24,  77 => 23,  74 => 22,  71 => 21,  69 => 20,  65 => 18,  63 => 14,  55 => 16,  52 => 15,  46 => 13,  44 => 10,  41 => 11,  39 => 10,  34 => 7,  26 => 4,  20 => 1,  31 => 6,  28 => 5,);
    }
}
